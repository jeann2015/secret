@extends('layouts.header')
@section('content')

@if(isset($notifys))

  <div class="alert alert-success" role="alert">
    <button class="close" aria-label="Close" data-dismiss="alert" type="button">
    <span aria-hidden="true">×</span>
    </button>
        @foreach ($notifys as $noti => $valor)
          @if($valor == 1)
            <strong>Well done!</strong> You successfully insert your Data.
          @endif
          @if($valor == 2)
            <strong>Well done!</strong> You successfully modified your Data.
          @endif
          @if($valor == 3)
            <strong>Well done!</strong> You successfully removed your Data,
          @endif
        @endforeach
      
  </div>
  
@endif

<table class="table table-striped">
  <tr>
      <td colspan="9">
          @foreach ($user_access as $user_acces)
              @if($user_acces->inserts == 1)
                <a href="{{ url('conductores/add') }}" class="btn btn-default" role="button">Add </a>
              @else
                <a href="#" class="btn btn-default" role="button">No Add  </a>
              @endif
          @endforeach
          
          <a href="{{ url('dashboard') }}" class="btn btn-default" role="button">Back </a>

      </td>
  </tr>
      <tr class="success">
          <td>Id</td>
          <td>Nombre</td>
          <td>Apellido</td>
          <td>Cedula</td>
          <td>Telefono1</td>
          <td>Telefono2</td>
          <td>Telefono3</td>
          <td>Modify</td>
          <td>Delete</td>
      </tr> 
         
          @foreach ($conductores as $conductore)
              <tr>
                  <td>{{ $conductore->id }}</td>  
                  <td>{{ $conductore->firtsname }}</td>  
                  <td>{{ $conductore->lastname }}</td>
                  <td>{{ $conductore->iddocument }}</td>
                  <td>{{ $conductore->phone1 }}</td>
                  <td>{{ $conductore->phone2 }}</td>
                  <td>{{ $conductore->phone3 }}</td>
                    @foreach ($user_access as $user_acces)
                      @if($user_acces->modifys == 1) 
                        <td><a href="conductores/mod/{{ $conductore->id }}" class="btn btn-success" role="button">Modify</a></td>
                      @else
                        <td><a href="#" class="btn btn-default" role="button">No Modify</a></td>
                      @endif
                      @if($user_acces->deletes==1) 
                        <td><a href="conductores/del/{{ $conductore->id }}" class="btn btn-danger" role="button">Delete</a></td>
                      @else
                        <td><a href="#" class="btn btn-default" role="button">No Delete</a></td>
                      @endif
                    @endforeach
              </tr>
          @endforeach
          {!! $conductores->render() !!}
  </table>    
@endsection
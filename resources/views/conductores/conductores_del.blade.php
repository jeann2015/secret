@extends('layouts.header')

@section('content')
{!! Form::open(array('url' => 'conductores/del')) !!}

  <table class="table">
    <tr>
        <td colspan="5">
            
            Conductores

        </td>
    </tr>
    <tr>
        <td colspan="5">
            
            <a href="{{ url('conductores') }}" class="btn btn-default" role="button">Back </a>

        </td>
    </tr>
        <tr>
            <td>Nombre</td>
            <td> {!! Form::text('firtsname',$conductores->firtsname,array('class' => 'form-control','id'=>'firtsname','required')) !!} </td>
        </tr>
        
        <tr>
            <td>Apellido</td>
            <td> {!! Form::text('lastname',$conductores->lastname,array('class' => 'form-control','id'=>'lastname','required')) !!} </td>
        </tr>
         <tr>
            <td>Cedula</td>
            <td> {!! Form::text('iddocument',$conductores->iddocument,array('class' => 'form-control','id'=>'iddocument','required')) !!} </td>
        </tr>
        <tr>
            <td>Telefono 1</td>
            <td> {!! Form::text('phone1',$conductores->phone1,array('class' => 'form-control','id'=>'phone1','required')) !!} </td>
        </tr>
        <tr>
            <td>Telefono 2</td>
            <td> {!! Form::text('phone2',$conductores->phone2,array('class' => 'form-control','id'=>'phone2','required')) !!} </td>
        </tr>   
        <tr>
            <td>Telefono 3</td>
            <td> {!! Form::text('phone3',$conductores->phone3,array('class' => 'form-control','id'=>'phone3','required')) !!} </td>
        </tr>  
         <tr>
            <td>Direccion</td>
            <td> {!! Form::textarea('address',$conductores->address,array('class' => 'form-control','id'=>'address','required')) !!} </td>
        </tr>    
        <tr>
            <td>Empresa</td>
            <td> {!! Form::select('id_company', $companys , $conductores->id_company, ['class'=>'form-control','required']); !!} </td>
        </tr>    
        <tr>
            <td colspan="2">
                {!! Form::submit('Delete!',array('class' => 'btn btn-primary','id'=>'delete')); !!}
                {!! Form::hidden('id',$conductores->id,array('id'=>'id')) !!}
            </td>
        </tr>         
    </table>   
  {!! Form::close() !!} 

@endsection
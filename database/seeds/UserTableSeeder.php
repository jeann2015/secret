<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Access;
use App\Module;
use App\Company;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create(['firtsname'=>'Jean Carlos','lastname'=>'Nuñez','user'=>'jeann','password'=>'a420384997c8a1a93d5a84046117c2aa','id_company'=>'1','status'=>'1']);

        Company::create(['name'=>'YellowCar', 'ruc'=>'RUC', 'phone'=>'Phone','address'=>'Panama']);

        Module::create([ 'description'=>'Principal', 'order'=>'1', 'id_father'=>'0','url'=>'#','status'=>'1','visible'=>'1']);
        Module::create([ 'description'=>'User', 'order'=>'1', 'id_father'=>'1','url'=>'user','status'=>'1','visible'=>'1']);
        Module::create([ 'description'=>'Company', 'order'=>'2', 'id_father'=>'1','url'=>'company','status'=>'1','visible'=>'1']);
        Module::create([ 'description'=>'Module', 'order'=>'3', 'id_father'=>'1','url'=>'module','status'=>'1','visible'=>'1']);
        Module::create([ 'description'=>'Change Password', 'order'=>'4', 'id_father'=>'1','url'=>'change','status'=>'1','visible'=>'1']);
        Module::create([ 'description'=>'Report', 'order'=>'2', 'id_father'=>'0','url'=>'#','status'=>'1','visible'=>'1']);
        Module::create([ 'description'=>'Report Conductores', 'order'=>'1', 'id_father'=>'6','url'=>'rconductores','status'=>'1','visible'=>'1']);
        Module::create([ 'description'=>'Conductores', 'order'=>'5', 'id_father'=>'1','url'=>'conductores','status'=>'1','visible'=>'1']);


        Access::create(['id_user'=>'1','id_module'=>'1','id_company'=>'1','views'=>'1','inserts'=>'1','modifys'=>'1','deletes'=>'1']);
        Access::create(['id_user'=>'1','id_module'=>'2','id_company'=>'1','views'=>'1','inserts'=>'1','modifys'=>'1','deletes'=>'1']);
        Access::create(['id_user'=>'1','id_module'=>'3','id_company'=>'1','views'=>'1','inserts'=>'1','modifys'=>'1','deletes'=>'1']);
        Access::create(['id_user'=>'1','id_module'=>'4','id_company'=>'1','views'=>'1','inserts'=>'1','modifys'=>'1','deletes'=>'1']);
        Access::create(['id_user'=>'1','id_module'=>'5','id_company'=>'1','views'=>'1','inserts'=>'1','modifys'=>'1','deletes'=>'1']);
        Access::create(['id_user'=>'1','id_module'=>'6','id_company'=>'1','views'=>'1','inserts'=>'1','modifys'=>'1','deletes'=>'1']);
        Access::create(['id_user'=>'1','id_module'=>'7','id_company'=>'1','views'=>'1','inserts'=>'1','modifys'=>'1','deletes'=>'1']);
        Access::create(['id_user'=>'1','id_module'=>'8','id_company'=>'1','views'=>'1','inserts'=>'1','modifys'=>'1','deletes'=>'1']);
    }
}

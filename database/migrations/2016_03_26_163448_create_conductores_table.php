<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConductoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('conductores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firtsname');
            $table->string('lastname');
            $table->string('iddocument');
            $table->string('phone1');
            $table->string('phone2');
            $table->string('phone3');
            $table->mediumText('address');
            $table->tinyInteger('id_company');
            $table->tinyInteger('id_user');
            $table->binary('foto');            
            $table->timestamps();
            $table->index(['id_user', 'id_company','iddocument']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
           Schema::drop('conductores');
    }
}

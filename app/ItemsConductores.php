<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsConductores extends Model
{
    protected $table = 'itemsconductores';

    protected $fillable = [
        'id_item', 'id_conductor', 'detalis'
    ];

     public $timestamps = true;
}

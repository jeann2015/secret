<?php

use App\User;
use App\Company;
use App\Module;
use App\Access;
use App\Conductores;
use Illuminate\Support\Collection as Collection;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => ['web']], function () {


		Route::get('/', function () {
		    return view('cover');
		});

		Route::get('/dashboard', function (Request $request) {
			
			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return redirect('login');
			}

			$module_principal = DB::table('modules')
			->select('modules.id', 
				'modules.description', 
				'modules.order', 
				'modules.id_father', 
				'modules.url', 
				'modules.messages', 
				'modules.status', 
				'modules.visible',
				'access.views', 
				'access.inserts', 
				'access.modifys', 
				'access.deletes')
			->join('access', function($join) use($iduser) {
			      $join->on('access.id_module', '=', 'modules.id')->where('access.id_user', '=',$iduser)
			      ->where('modules.url', '=','#');
			})->orderBy('modules.order')->orderBy('modules.id')->get();	

			$module_menu = DB::table('modules')
			->select('modules.id', 
				'modules.description', 
				'modules.order', 
				'modules.id_father', 
				'modules.url', 
				'modules.messages', 
				'modules.status', 
				'modules.visible',
				'access.views', 
				'access.inserts', 
				'access.modifys', 
				'access.deletes')
			->join('access', function($join) use($iduser) {
			      $join->on('access.id_module', '=', 'modules.id')->where('access.id_user', '=',$iduser)
			      ->where('modules.url', '<>','#');
			})->orderBy('modules.order')->get();	
		    
		    $module_principals = Collection::make($module_principal);
		    $module_menus = Collection::make($module_menu);

		    $users = User::find($iduser);


		    return view('dashboard.dashboard',compact('module_principals','module_menus','users'));

		});

		Route::get('/cover', function () {
		    return view('cover');
		});





		/*Users*/

		Route::get('user',function(Request $request){

			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}

			$users = User::all();
			$companys = Company::all();
			
			$module = new Module;  
			$url = $request->path();
			$user_access = $module->accesos($iduser,$url);

			$noti_value = $request->session()->get('notify');
			
			if($noti_value<>"" && $noti_value<>null){
				$request->session()->forget('notify');
				$notifys['notify'] = $noti_value;
			}

			return view('users.users',compact('users','companys','user_access','notifys'));
		});

		Route::get('user/add',function(){

			$module = DB::table('modules')
				->select('modules.id', 
					'modules.description', 
					'modules.order', 
					'modules.id_father', 
					'modules.url', 
					'modules.messages', 
					'modules.status', 
					'modules.visible')
				->leftjoin('access', function($join){
				      $join->on('access.id_module', '=', 'modules.id');
				})->get();	
			    
			    $modules = Collection::make($module);
				
				foreach ($modules as $module){
			    	
		    		$arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'0');
		    	
		    		$arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'0');
		    	
		    		$arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'0');
		    	
		    		$arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'0');

			    }


			$companys = DB::table('companys')->pluck('name', 'id');	
			return view('users.users_add',compact('companys','modules','arr_view_arr','arr_save_arr','arr_modify_arr','arr_delete_arr'));
		});

		Route::get('user/mod/{id}',function($id,Request $request){

			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}
			
			$users = User::find($id);
			$module = DB::table('modules')
			->select('modules.id', 
				'modules.description', 
				'modules.order', 
				'modules.id_father', 
				'modules.url', 
				'modules.messages', 
				'modules.status', 
				'modules.visible',
				'access.views', 
				'access.inserts', 
				'access.modifys', 
				'access.deletes')
			->leftjoin('access', function($join) use($id) {
			      $join->on('access.id_module', '=', 'modules.id')->where('access.id_user', '=',$id);
			})->get();	
		    
		    $modules = Collection::make($module);
			$companys = DB::table('companys')->pluck('name', 'id');	

			foreach ($modules as $module)
		    {

		    	if($module->views == 1){
		    		$arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'1');
		    	}else{
		    		$arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'0');
		    	}

		    	if($module->inserts == 1){
		    		$arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'1');
		    	}else{
		    		$arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'0');
		    	}

		    	if($module->modifys == 1){
		    		$arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'1');
		    	}else{
		    		$arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'0');
		    	}

				if($module->deletes == 1){
		    		$arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'1');
		    	}else{
		    		$arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'0');
		    	}
		    	

		    }

			return view('users.users_mod',compact('users','companys','modules','arr_view_arr','arr_save_arr','arr_modify_arr','arr_delete_arr'));
		});

		Route::get('user/del/{id}',function($id, Request $request){

			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}

			$module = DB::table('modules')
				->select('modules.id', 
					'modules.description', 
					'modules.order', 
					'modules.id_father', 
					'modules.url', 
					'modules.messages', 
					'modules.status', 
					'modules.visible',
					'access.views', 
					'access.inserts', 
					'access.modifys', 
					'access.deletes')
				->leftjoin('access', function($join) use($id) {
				      $join->on('access.id_module', '=', 'modules.id')->where('access.id_user', '=',$id);
				})->get();	

			$modules = Collection::make($module);
			
			foreach ($modules as $module)
		    {

		    	if($module->views == 1){
		    		$arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'1');
		    	}else{
		    		$arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'0');
		    	}

		    	if($module->inserts == 1){
		    		$arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'1');
		    	}else{
		    		$arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'0');
		    	}

		    	if($module->modifys == 1){
		    		$arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'1');
		    	}else{
		    		$arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'0');
		    	}

				if($module->deletes == 1){
		    		$arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'1');
		    	}else{
		    		$arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'0');
		    	}
		    	

		    }

			$users = User::find($id);
			$companys = DB::table('companys')->pluck('name', 'id');	
			return view('users.users_del',compact('users','companys','modules','arr_view_arr','arr_save_arr','arr_modify_arr','arr_delete_arr'));
		});

		Route::post('user/add','UserController@save');
		Route::post('user/mod','UserController@update');
		Route::post('user/del','UserController@delete');

		/*Users*/

		/*Companys*/
		Route::get('company',function(Request $request){

			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser ==""){
				return view('login.login');
			}
			
			$module = new Module; 
			$url = $request->path();
			$user_access = $module->accesos($iduser,$url);

			$noti_value = $request->session()->get('notify');
			
			if($noti_value<>"" && $noti_value<>null){
				$request->session()->forget('notify');
				$notifys['notify'] = $noti_value;
			}

			$companys = Company::all();
			return view('companys.companys',compact('companys','user_access','notifys'));
		});

		Route::get('company/add',function(Request $request){
			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}

			return view('companys.companys_add');
		});

		Route::get('company/mod/{id}',function($id, Request $request){
			
			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}

			$companys = Company::find($id);	
			return view('companys.companys_mod',compact('companys'));

		});

		Route::get('company/del/{id}',function($id,Request $request){

			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}

			$companys = Company::find($id);	
			return view('companys.companys_del',compact('companys'));

		});

		Route::post('company/add','CompanyController@save');
		Route::post('company/mod','CompanyController@update');
		Route::post('company/del','CompanyController@delete');
		/*Companys*/

		/*Modules*/
		Route::get('module',function(Request $request){
			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}
			
			$module = new Module; 
			$url = $request->path();
			$user_access = $module->accesos($iduser,$url);
			$modules = Module::all();

			$noti_value = $request->session()->get('notify');
			
			if($noti_value<>"" && $noti_value<>null){
				$request->session()->forget('notify');
				$notifys['notify'] = $noti_value;
			}

			return view('modules.modules',compact('modules','user_access','notifys'));
		});

		Route::get('module/add',function(Request $request){
			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}

			return view('modules.modules_add');
		});

		Route::get('module/mod/{id}',function($id,Request $request){
			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}

			$modules = Module::find($id);	
			return view('modules.modules_mod',compact('modules'));

		});

		Route::get('module/del/{id}',function($id,Request $request){
			
			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}

			$modules = Module::find($id);	
			return view('modules.modules_del',compact('modules'));

		});

		Route::post('module/add','ModuleController@save');
		Route::post('module/mod','ModuleController@update');
		Route::post('module/del','ModuleController@delete');

		/*Modules*/

		/*Login*/
		Route::post('login','UserController@login');
		Route::get('logout','UserController@logout');

		Route::get('/login', function () {
		    return view('login.login');
		});

		/*Login*/

		/*ChangePass*/
		Route::get('/change', function (Request $request) {
			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}

		    return view('change.change');
		});

		Route::post('change','UserController@change');
		/*ChangePass*/

		/*Conductores*/

		Route::get('conductores',function(Request $request){

			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}
			
			$module = new Module; 
			$url = $request->path();
			$user_access = $module->accesos($iduser,$url);
			$conductores = Conductores::paginate();

			$noti_value = $request->session()->get('notify');
			
			if($noti_value<>"" && $noti_value<>null){
				$request->session()->forget('notify');
				$notifys['notify'] = $noti_value;
			}

			$companys = DB::table('companys')->pluck('name', 'id');	
			return view('conductores.conductores',compact('conductores','user_access','notifys','companys'));
		});

		Route::get('conductores/add',function(Request $request){
			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}
			
			$companys = DB::table('companys')->pluck('name', 'id');	
			return view('conductores.conductores_add',compact('companys'));
		});

		Route::get('conductores/mod/{id}',function($id,Request $request){
			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}

			$conductores = Conductores::find($id);	
			$companys = DB::table('companys')->pluck('name', 'id');	
			return view('conductores.conductores_mod',compact('conductores','companys'));

		});

		Route::get('conductores/del/{id}',function($id,Request $request){
			
			$user = $request->session()->get('user');
			$iduser = $request->session()->get('id');

			if($user=="" && $iduser==""){
				return view('login.login');
			}

			$conductores = Conductores::find($id);	
			$companys = DB::table('companys')->pluck('name', 'id');	
			return view('conductores.conductores_del',compact('conductores','companys'));

		});

		Route::post('conductores/add','ConductoresController@save');
		Route::post('conductores/mod','ConductoresController@update');
		Route::post('conductores/del','ConductoresController@delete');

		/*Conductores*/

});
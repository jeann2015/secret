<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Conductores;


class ConductoresController extends Controller
{
    public function save(Request $request)
    {
        $user = $request->session()->get('user');
        $iduser = $request->session()->get('id');

        if($user=="" && $iduser==""){
            return view('login.login');
        }

    	$conductores = new Conductores;       
   
    	$conductores->firtsname = $request->firtsname;
    	$conductores->lastname = $request->lastname;
    	$conductores->phone1 = $request->phone1;
    	$conductores->phone2 = $request->phone2;
    	$conductores->phone3 = $request->phone3;
    	$conductores->address = $request->address;
    	$conductores->id_company = $request->id_company;
    	$conductores->id_user = $iduser;
        $conductores->iddocument = $request->iddocument;
    	$conductores->save();

        $request->session()->put('notify', '1');
        return redirect('conductores');

    }

    public function update(Request $request)
    {
        $user = $request->session()->get('user');
        $iduser = $request->session()->get('id');

        if($user=="" && $iduser==""){
            return view('login.login');
        }

        $conductores = Conductores::find($request->id);

        $conductores->firtsname = $request->firtsname;
    	$conductores->lastname = $request->lastname;
    	$conductores->phone1 = $request->phone1;
    	$conductores->phone2 = $request->phone2;
    	$conductores->phone3 = $request->phone3;
    	$conductores->address = $request->address;
    	$conductores->id_company = $request->id_company;
    	$conductores->id_user = $iduser;
        $conductores->iddocument = $request->iddocument;
    	$conductores->save();
    	
        $request->session()->put('notify', '2');
        return redirect('conductores');

    }

    public function delete(Request $request)
    {
        $user = $request->session()->get('user');
        $iduser = $request->session()->get('id');

        if($user=="" && $iduser==""){
            return view('login.login');
        }
        
        $conductores = Conductores::find($request->id);;

        $conductores->delete();

    	$request->session()->put('notify', '3');
        return redirect('conductores');

    }


}

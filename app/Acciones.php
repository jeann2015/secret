<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acciones extends Model
{
     protected $table = 'acciones';

    protected $fillable = ['description', 'ponderacion'];

     public $timestamps = true;
}

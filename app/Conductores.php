<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conductores extends Model
{
    protected $table = 'conductores';

    protected $fillable = [
        'firtsname', 'lastname', 'phone1','phone2','phone3','address','id_company','id_user','iddocument','foto'
    ];

     public $timestamps = true;
}
